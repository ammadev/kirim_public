import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

import Login from './Login';
import SignUp from './SignUp';

const App = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      headerShown:false
    },
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: {
      headerShown:false
    },
  },
  
});
export default createAppContainer(App);