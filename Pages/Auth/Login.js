import React from 'react';
import { View, StyleSheet, Text, Dimensions, Image,TouchableOpacity } from 'react-native';
import TextInputClear from '../../Components/TextInputClear';
import ButtonSubmitArrow from '../../Components/ButtonSubmitArrow';

const DeviceWidth = Dimensions.get('window').width;

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);

  }
  render() {
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1,backgroundColor:'grey' }}>
        <Image source={require('../../Assets/Logo/KIRIM_Text.png')} style={styles.logo}/>
        <View style={[styles.card]}>
          <Text style={[styles.headerCardText]}>LOGIN</Text>
        <TextInputClear
        placeholder="Enter Email"
        />
        <TextInputClear
        placeholder="Enter Password"
        />
        <TouchableOpacity style={[styles.forgotTouch]} >
        <Text style={[styles.footerCardText]}>FORGOT PASSWORD?</Text>
        </TouchableOpacity>
        </View>
        <ButtonSubmitArrow
        style={[styles.btnSubmit]}
        title="SIGN IN"/>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  card:{
    backgroundColor:'white',
    width: DeviceWidth*0.8,
    padding:25,
  },
  headerCardText:{
    color:'#000066ff',
    fontSize:20,
    fontWeight:'700'
  },
  footerCardText:{
    color:'#424242ff',
    fontSize:13,
    fontWeight:'700'
  },
  forgotTouch:{
    marginTop:15,
  },
  btnSubmit:{
    width: DeviceWidth*0.8,
  },
  logo: {
    width: 80,
    height: 80,
    marginBottom:DeviceWidth*0.15
  }
});