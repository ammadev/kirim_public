import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import Navbar from '../../Components/Navbar';

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            screen: 'personal'
        }

    }
    render() {
        
        if (this.state.screen == 'personal') {
            return (
                <View>
                    <Navbar
                        title="Create Account"
                        customClick={() => this.props.navigation.navigate('Login')}
                    />
                    <TouchableOpacity onPress={() => this.setState({ screen: 'company' })} style={{ backgroundColor: 'grey' }}>
                        <Text>Klik Untuk Company</Text>
                    </TouchableOpacity>
                    <Text>Personal Sign Up</Text>

                </View>
            );
        } else {
            return (
                <View>
                <Navbar
                    title="Create Account"
                     customClick={() => this.props.navigation.navigate('Login')}
                />
                    <TouchableOpacity onPress={() => this.setState({ screen: 'personal' })} style={{ backgroundColor: 'grey' }}>
                        <Text>Klik Untuk Personal</Text>
                    </TouchableOpacity>
                    <Text>Company Sign Up</Text>
                </View>
            );
        }
    }
}


const styles = StyleSheet.create({

});