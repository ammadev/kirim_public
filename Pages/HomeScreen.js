import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import Navbar from '../Components/NavbarHome';
import ButtonMenu from '../Components/ButtonMenu';

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      userSelected: [],
      data: [
        { id: 1, text: "Ship", navigate: 'AppShip', src: require('../Assets/Icon/Circle.png') },
        { id: 2, text: "Shipment List", navigate: '', src: require('../Assets/Icon/Square.png') },
        { id: 3, text: "Quick Tools", navigate: '', src: require('../Assets/Icon/Triangle.png') },
        { id: 4, text: "Notifications", navigate: '', src: require('../Assets/Icon/Triangle.png') },
        { id: 5, text: "Product Services", navigate: '', src: require('../Assets/Icon/Circle.png') },
        { id: 6, text: "Manage", navigate: '', src: require('../Assets/Icon/Square.png') },
        { id: 7, text: "Locations", navigate: 'AppLocations', src: require('../Assets/Icon/Square.png') },
        { id: 8, text: "Share", navigate: '', src: require('../Assets/Icon/Triangle.png') },
        { id: 8, text: "Contact Us", navigate: '', src: require('../Assets/Icon/Circle.png') },
      ]
    };
  }

  render() {
    return (
      <View>
        <Navbar
          title="K I R I M"
          customClick={() => this.props.navigation.navigate('AppAuth')} />
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <FlatList
            data={this.state.data}
            keyExtractor={(item) => {
              return item.id;
            }}
            renderItem={({ item }) => {
              return (
                <ButtonMenu
                  images={item.src}
                  customClick={() => this.props.navigation.navigate(item.navigate)}
                  title={item.text}
                />
              )
            }}
            numColumns={3}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});