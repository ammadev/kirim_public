import React from 'react';
import { View, StyleSheet, Text, FlatList, Dimensions, TouchableOpacity, Image } from 'react-native';
import Navbar from '../../Components/Navbar';
import TextInputSearch from '../../Components/TextInputSearch';

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            userSelected: [],
            data: [
                { id: 1, title: "Gajahmada", address: "232, Yukon Street", city: "Bay North 601", distance: "0.1" },
                { id: 2, title: "Tanjungpura", address: "212, Humble Street", city: "Bay East 405", distance: "0.2" },
                { id: 3, title: "Sunter", address: "246, Ranger Street", city: "Bay West 306", distance: "0.5" },
            ]
        };
    }
    render() {
        return (
            <View>
                <Navbar
                    title="KIRIM Location"
                    customClick={() => this.props.navigation.navigate('HomeScreen')}
                />
                <TextInputSearch
                    placeholder="Find Kirim Location"
                />
                <FlatList
                    data={this.state.data}
                    keyExtractor={(item) => {
                        return item.id;
                    }}
                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity style={styles.card} onPress={() => {
                                this.props.navigation.navigate('LocationMaps', {
                                    title: item.title, address: item.address, distance: item.distance
                                });
                            }}>
                                <View style={styles.listItem}>
                                    <View style={{ flex: 1 }}>
                                        <Text style={[styles.title]}>KIRIM {item.title}</Text>
                                        <Text style={[styles.address]}>{item.address}</Text>
                                        <Text style={[styles.city]}>{item.city}</Text>
                                    </View>
                                    <TouchableOpacity style={{ height: 50, width: 50, justifyContent: "center", alignItems: "center" }}>
                                        <Text style={[styles.distance]}>{item.distance} KM</Text>
                                    </TouchableOpacity>
                                </View>

                            </TouchableOpacity>

                        )
                    }}
                    numColumns={1}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    listItem: {
        borderBottomColor: 'grey',
        borderBottomWidth: 0.5,
        paddingHorizontal: 8,
        paddingTop: 10,
        paddingBottom:8,
        width: "95%",
        flex: 1,
        alignSelf: "center",
        flexDirection: "row",
        borderRadius: 5
    },
    title: {
        fontSize: 15,
    },
    address: {
        fontSize: 12,
        marginVertical:2,
        color: 'grey'
    },
    city: {
        fontSize: 12,
        color: 'grey'
    },
    distance: {
        fontSize: 15,
        color: "#000066ff",
    },
});