import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

import Location from './Location';
import LocationMaps from './LocationMaps';

const App = createStackNavigator({
  Location: {
    screen: Location,
    navigationOptions: {
      headerShown:false
    },
  },
  LocationMaps: {
    screen: LocationMaps,
    navigationOptions: {
      headerShown:false
    },
  },
});
export default createAppContainer(App);