import React from 'react';
import { View, StyleSheet, Text, Dimensions, ScrollView } from 'react-native';
import Navbar from '../../Components/Navbar';

const DeviceHeight = Dimensions.get('window').height;

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.state = {
            title: this.params.title,
            address: this.params.address,
            distance: this.params.distance,
        };
    }
    render() {
        return (
            <ScrollView>
                <View>
                    <Navbar
                        title="KIRIM Location"
                        customClick={() => this.props.navigation.navigate('Location')}
                    />
                    <View style={[styles.maps]}><Text>Tampilan Maps</Text></View>
                    <View style={[styles.card]}>

                        <View style={{ flex: 1 }}>
                            <Text style={[styles.title]}>KIRIM {this.state.title}</Text>
                            <Text style={[styles.address]}>{this.state.address}</Text>
                        </View>
                        <View style={{ height:30 , width: 50, justifyContent: "center", alignItems: "center" }}>
                            <Text style={[styles.distance]}>{this.state.distance} KM</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    maps: {
        height: DeviceHeight * 0.55,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'grey'
    },
    card: {
        height: DeviceHeight * 0.35,
        padding: 20,
        flex:1,
        flexDirection: "row",
    },
    title: {
        fontSize: 15,
    },
    address: {
        fontSize: 12,
        marginVertical:2,
        color: 'grey'
    },
    city: {
        fontSize: 12,
        color: 'grey'
    },
    distance: {
        fontSize: 15,
        color: "#000066ff",
    },

});