import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image} from 'react-native';

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={[styles.container]}>
        <TouchableOpacity  onPress={() => this.props.navigation.navigate('HomeScreen')}>
          <Image source={require('../Assets/Logo/KIRIM_Text.png')} style={styles.logo} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 100,
    height: 100
  }
});