import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Navbar from '../../Components/Navbar';

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <View>
                <Navbar
                    title="Ship"
                    customClick={() => this.props.navigation.navigate('HomeScreen')}
                />
                <View style={{ alignItems: 'center' }}>
                    <Text>Ini Ship</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

});