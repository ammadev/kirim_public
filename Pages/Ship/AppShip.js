import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

import Ship from './Ship';

const App = createStackNavigator({
  Ship: {
    screen: Ship,
    navigationOptions: {
      headerShown:false
    },
  },
  
});
export default createAppContainer(App);