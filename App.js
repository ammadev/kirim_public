import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

import HomeScreen from './Pages/HomeScreen';
import SplashScreen from './Pages/SplashScreen';
import AppShip from './Pages/Ship/AppShip';
import AppAuth from './Pages/Auth/AppAuth';
import AppLocations from './Pages/Locations/AppLocations';

const App = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen,
    navigationOptions: {
      headerShown:false,
    },
  },
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions: {
      headerShown:false
    },
  },
  AppShip: {
    screen: AppShip,
    navigationOptions: {
      headerShown:false
    },
  },
  AppAuth: {
    screen: AppAuth,
    navigationOptions: {
      headerShown:false
    },
  },
  AppLocations: {
    screen: AppLocations,
    navigationOptions: {
      headerShown:false
    },
  },
});
export default createAppContainer(App);