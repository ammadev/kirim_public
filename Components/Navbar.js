import React from 'react';
import { View, Text, StyleSheet,TouchableOpacity,Image } from 'react-native';

const Navbar = props => {
  return (
    <View style={[styles.navBody]}>
      <TouchableOpacity style={[styles.buttonLeft]} onPress={props.customClick}>
      <Image source={require('../Assets/Icon/ButtonBack.png')} style={styles.icon} />
      </TouchableOpacity>
      <Text style={[styles.navText]}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  navBody: {
    top: 0,
    alignContent: 'center',
    alignItems: 'center',
    paddingVertical: 12,
    borderBottomWidth: 0.5,
  },
  navText: {
    fontSize: 20,
    color: '#000066ff',
    fontWeight: '700'
  },
  buttonLeft:{
    width:25,
    height:20,
    position:'absolute',
    left:5,
    marginVertical:12,
    marginHorizontal:12,
    borderRadius:10
  },
  icon:{
    width:25,
    height:20,
  }
});

export default Navbar;