import React from 'react';
import { View, TextInput } from 'react-native';
const TextInputClear = props => {
  return (
    <View
      style={{
        marginVertical: 5,
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
      }}>
      <TextInput
        underlineColorAndroid="transparent"
        placeholder={props.placeholder}
        placeholderTextColor="grey"
        keyboardType={props.keyboardType}
        onChangeText={props.onChangeText}
        returnKeyType={props.returnKeyType}
        numberOfLines={props.numberOfLines}
        multiline={props.multiline}
        onSubmitEditing={props.onSubmitEditing}
        style={props.style}
        blurOnSubmit={false}
        value={props.value}
      />
    </View>
  );
};
export default TextInputClear;