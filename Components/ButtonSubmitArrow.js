import React from 'react';
import { TouchableOpacity, Text, StyleSheet,Image } from 'react-native';

const ButtonSubmitArrow = props => {
  return (
    <TouchableOpacity style={[styles.button,props.style]} onPress={props.customClick}>
      <Text style={styles.text}>{props.title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#000066ff',
    paddingHorizontal:25,
    paddingVertical:15
  },
  text: {
    fontSize:12,
    color: 'white',
  },
  image:{
    marginVertical:5,
    width:45,
    height:45
  }
});
export default ButtonSubmitArrow;