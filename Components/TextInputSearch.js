import React from 'react';
import { View, TextInput,StyleSheet } from 'react-native';
const TextInputSearch = props => {
  return (
    <View
      style={{
        marginTop: 15,
        marginHorizontal:10,
        shadowColor: '#00000021',
        shadowOffset: {
          width: 0,
          height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,
      }}>
      <TextInput
        underlineColorAndroid="transparent"
        placeholder={props.placeholder}
        placeholderTextColor="grey"
        keyboardType={props.keyboardType}
        onChangeText={props.onChangeText}
        returnKeyType={props.returnKeyType}
        numberOfLines={props.numberOfLines}
        multiline={props.multiline}
        onSubmitEditing={props.onSubmitEditing}
        style={[styles.input,props.style]}
        blurOnSubmit={false}
        value={props.value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
input:{
    backgroundColor:'white',
    paddingHorizontal:10,
}
});

export default TextInputSearch;