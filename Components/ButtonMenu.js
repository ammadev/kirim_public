import React from 'react';
import { TouchableOpacity, Text, StyleSheet,Dimensions,Image } from 'react-native';

const DeviceWidth = Dimensions.get('window').width;

const ButtonMenu = props => {
  return (
    <TouchableOpacity style={styles.button} onPress={props.customClick}>
      <Image source={props.images} style={styles.image} />
      <Text style={styles.text}>{props.title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#000066ff',
    paddingHorizontal: 5,
    paddingVertical:10,
    marginHorizontal:8,
    marginVertical:8,
    width:DeviceWidth*0.27,
    height:DeviceWidth*0.27,
  },
  text: {
    fontSize:12,
    color: 'white',
  },
  image:{
    marginVertical:5,
    width:45,
    height:45
  }
});
export default ButtonMenu;