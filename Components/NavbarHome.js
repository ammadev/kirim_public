import React from 'react';
import { View, Text, StyleSheet,TouchableOpacity } from 'react-native';

const Navbar = props => {
  return (
    <View style={[styles.navBody]}>
      <Text style={[styles.navText]}>{props.title}</Text>
      <TouchableOpacity style={[styles.buttonRight]} onPress={props.customClick}>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  navBody: {
    top: 0,
    alignContent: 'center',
    alignItems: 'center',
    paddingVertical: 12,
    borderBottomWidth: 0.5,
  },
  navText: {
    fontSize: 20,
    color: '#000066ff',
    fontWeight: '700'
  },
  buttonRight:{
    width:20,
    height:20,
    position:'absolute',
    right:5,
    marginVertical:12,
    marginHorizontal:12,
    backgroundColor: '#000066ff',
    borderRadius:10
  }
});

export default Navbar;